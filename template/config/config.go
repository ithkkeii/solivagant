package config

import (
	"os"

	"github.com/joho/godotenv"
)

var EnvMap = map[string]string{
	"PORT":        "PORT",
	"JWT_SECRET":  "JWT_SECRET",
	"DB_USER":     "DB_USER",
	"DB_PASSWORD": "DB_PASSWORD",
	"DB_HOST":     "DB_HOST",
	"DB_NAME":     "DB_NAME",
	"DB_PORT":     "DB_PORT",
}

var PORT string
var JWT_SECRET string
var DB_USER string
var DB_PASSWORD string
var DB_HOST string
var DB_NAME string
var DB_PORT string

func checkExist(key string) {
	data := os.Getenv(key)
	if data == "" {
		panic("Missing env: " + key)
	}
}

func LoadEnv() {
	__DEV__ := os.Getenv("APP_ENV") != "production"

	envFile := ".env"
	if __DEV__ {
		envFile = ".env.dev"
	}

	err := godotenv.Load(envFile)
	if err != nil {
		panic("Error loading .env file")
	}

	PORT = os.Getenv("PORT")
	JWT_SECRET = os.Getenv("JWT_SECRET")
	DB_USER = os.Getenv("DB_USER")
	DB_PASSWORD = os.Getenv("DB_PASSWORD")
	DB_HOST = os.Getenv("DB_HOST")
	DB_NAME = os.Getenv("DB_NAME")
	DB_PORT = os.Getenv("DB_PORT")

	for _, env := range EnvMap {
		checkExist(env)
	}
}
