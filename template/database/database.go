package database

import (
	"fmt"
	"main/config"
	"main/log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var Conn *sqlx.DB

func Connect() {
	// TODO: what is sslmode ?
	dataSource := fmt.Sprintf(
		"host=%s dbname=%s user=%s password=%s port=%s sslmode=disable",
		config.DB_HOST,
		config.DB_NAME,
		config.DB_USER,
		config.DB_PASSWORD,
		config.DB_PORT,
	)

	conn, err := sqlx.Connect("postgres", dataSource)
	if err != nil {
		log.Logger.Error().Err(err).Msg("Fail to connect to database")
		panic(err)
	}

	Conn = conn
}
