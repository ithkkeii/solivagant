package log

import (
	"os"

	"github.com/rs/zerolog"
)

var Logger zerolog.Logger

func CreateFileLogger() {
	curDir, err := os.Getwd()
	if err != nil {
		panic("Fail to get current dir - to initialize logger")
	}

	file, err := os.OpenFile(curDir+"/log/errors.txt", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		panic("Fail to initialize logger" + err.Error())
	}

	Logger = zerolog.New(file).With().Logger()
}
