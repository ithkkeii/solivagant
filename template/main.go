package main

import (
	"main/config"
	"main/log"
	"main/modules/ping"
	"main/modules/user"

	"github.com/gin-gonic/gin"
)

func main() {
	log.CreateFileLogger()
	config.LoadEnv()
	// database.Connect()

	router := gin.Default()

	apiV1 := router.Group("/v1")

	apiV1.GET("/ping", ping.Handler)
	apiV1.GET("/users", user.FindAll)

	router.Run()
}
