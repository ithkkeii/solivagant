package main

import (
	"fmt"
	"main/jwt"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type signInInput struct {
	Name string `json:"name"`
}

func main() {
	r := gin.Default()

	conn, err := sqlx.Connect("postgres", "host=localhost user=postgres password=password port=5433 dbname=users-service sslmode=disable")
	if err != nil {
		// TODO: change to debug print
		fmt.Println("db connection error")
		fmt.Println(err.Error())
	}

	r.POST("/sign-in", func(c *gin.Context) {
		var input signInInput

		if err := c.BindJSON(&input); err != nil {
			c.JSON(200, gin.H{
				"message": "pong",
			})
			return
		}

		type User struct {
			ID   string `db:"id"`
			Name string `db:"name"`
		}

		var user User
		// TODO: this query result in error empty rows if where clause got no value
		err := conn.Get(&user, fmt.Sprintf("SELECT * FROM users WHERE users.name = '%s'", input.Name))
		if err != nil {
			fmt.Println("sql error")
			fmt.Println(err.Error())
			c.JSON(400, gin.H{
				"message": "fail",
			})
			return
		}

		token, err := jwt.GenerateJWT(user.ID)
		if err != nil {
			c.JSON(400, gin.H{
				"message": "bad request",
			})
			return
		}

		// TODO: check secure in prod
		c.SetCookie("auth-cookie", token, 3600, "/", "localhost", false, true)

		c.JSON(200, gin.H{
			"message": token,
		})
	})

	r.Run() // listen and serve on 0.0.0.0:8080
}
