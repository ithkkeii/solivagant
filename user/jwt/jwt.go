package jwt

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type JWTCustomClaim struct {
	jwt.RegisteredClaims

	UserID string `json:"userId"`
}

func GenerateJWT(userID string) (string, error) {
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, &JWTCustomClaim{
		UserID: userID,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 3)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	})

	// TODO: move this secret
	token, err := t.SignedString([]byte("key"))
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return token, nil
}
