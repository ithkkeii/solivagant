package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type User struct {
	ID   string `db:"id"`
	Name string `db:"name"`
}

type Room struct {
	ID    string `db:"id"`
	Title string `db:"title"`
}

type Message struct {
	ID     string `db:"id"`
	Text   string `db:"text"`
	UserID string `db:"user_id"`
	User   User   `db:"user"`
	RoomID string `db:"room_id"`
}

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	conn, err := sqlx.Connect("postgres", "host=localhost user=postgres password=password port=5433 dbname=chat sslmode=disable")
	if err != nil {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		log.Debug().Err(err).Send()
	}

	r := gin.Default()

	r.GET("/rooms", func(ctx *gin.Context) {
		var rooms []Room

		err := conn.Select(&rooms, "SELECT * FROM rooms")
		if err != nil {
			log.Info().Str("/rooms", "sql error").Err(err).Send()
			ctx.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
			return
		}

		ctx.JSON(http.StatusOK, rooms)
	})

	r.GET("/rooms/:id", func(ctx *gin.Context) {
		roomId := ctx.Param("id")

		messages := []Message{}

		err := conn.Select(&messages, fmt.Sprintf(`SELECT messages.*, u.id AS "user.id", u.name AS "user.name" FROM messages INNER JOIN users u ON u.id = messages.user_id WHERE messages.room_id = '%s'`, roomId))
		if err != nil {
			log.Info().Str("/messages", "sql error").Err(err).Send()
			ctx.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
			return
		}

		ctx.JSON(http.StatusOK, messages)
	})

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
